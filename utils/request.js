const log = require('log.js')
const postJson = function (relativeUrl, params = null, authorization = null, showError = true) {
  return new Promise((resolve, reject) => {
    let requestParam = {};
    requestParam.url = "http://localhost:8080/" + relativeUrl;
    requestParam.method = "POST";
    if (params != null) {
      requestParam.data = params;
    }
    requestParam.header = {
      'content-type': 'application/json'
    }
    if (authorization != null) {
      requestParam.header["Authorization"] = authorization;
    }
    //成功的处理
    requestParam.success = function (res) {
      if (res.data.rtnCode !== "SUCCESS") {
        log.log("请求数据失败:" + res.data.errorMessage);
        if (showError) {
          wx.showToast({
            icon:'none',
            title: res.data.errorMessage,
            mask: true
          });
        }
        resolve(null);
      }
      resolve(res.data);
    }
    //失败的处理
    requestParam.fail = function (res) {
      wx.showToast({
        icon: 'none',
        title: "网络请求失败",
        mask: true
      })
    }
    wx.request(requestParam);
  });
}
module.exports = {
  postJson: postJson
}