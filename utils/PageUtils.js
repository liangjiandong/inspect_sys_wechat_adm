const app = getApp();
class PageUtils {
  page = {
    pageNum: 1,
    pageSize: 10,
    hasMoreData: true
  }
  /**
   * 请求的url
   */
  requestRelativeUrl = null;
  requestParams = null;
  constructor(requestRelativeUrl, requestParams = null) {
    this.requestRelativeUrl = requestRelativeUrl;
    this.requestParams = requestParams;
  }
  /**
   * 获取数据列表
   */
  getDataList(dataList, refresh = false) {
    let params = { "pageNum": this.page.pageNum, "pageSize": this.page.pageSize };
    if (this.requestParams != null) {
      params = Object.assign(params, requestParams);
    }
    let pageInfo = this.page;
    return app.request.postJson(this.requestRelativeUrl, params, app.globalData.token).then(res => {
      if (refresh) {
        wx.stopPullDownRefresh();
      }
      let resultList = res.rtnData;
      if (resultList.length > 0) {
        dataList = dataList.concat(resultList);
      }
      if (resultList.length < pageInfo.pageNum) {
        pageInfo.hasMoreData = false;
      } else {
        pageInfo.hasMoreData = true;
        pageInfo.pageNum = pageInfo.pageNum + 1;
      }
      return dataList;
    });
  }
  /**
   * 刷新页面,同时返回数据列表
   */
  pullDownRefresh(dataList) {
    this.page.pageNum = 1
    return this.getDataList([], true);
  }
  /**
   * 到达底部
   */
  reachBottom(dataList) {
    if (this.page.hasMoreData) {
      return this.getDataList(dataList);
    } else {
      app.util.showToast("没有更多数据");
      return null;
    }
  }

}
export default PageUtils