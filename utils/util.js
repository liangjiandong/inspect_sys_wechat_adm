const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

/**
 * 验证手机号码是否正确
 */
const validatePhoneFormat = (phone)=> {
  var myreg = /^[1][3,4,5,7,8][0-9]{9}$/;
  if (!myreg.test(phone)) {
    return false;
  } else {
    return true;
  }
}
/**
 * 微信小程序弹框
 */
const showToast=(msg, icon="none",duration = 2000)=>{
  wx.showToast({
    title: msg,
    duration: duration,
    icon: icon
  })
}
module.exports = {
  formatTime: formatTime,
  validatePhoneFormat: validatePhoneFormat,
  showToast: showToast
}
