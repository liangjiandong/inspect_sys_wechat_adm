//app.js
import Touches from './utils/Touches.js'
App({
  login: function (usrInfo) {
    // 登录
    wx.login({
      success: res => {
        let params = {
          "nickName": usrInfo.nickName,
          "avatarUrl": usrInfo.avatarUrl,
          "code": res.code
        };
        this.request.postJson("auth/wechat/adm", params, null, false).then(data => {
          if (data == null) {
            wx.redirectTo({
              url: '/pages/login/phone-login',
            });
          } else {
            this.globalData.token = data.rtnData;
          }
          if (this.userInfoReadyCallback) {
            this.userInfoReadyCallback(res)
          }
        });
      }
    })
  },
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo
              this.login(res.userInfo);
            }
          })
        } else {
          wx.redirectTo({
            url: '/pages/login/phone-login',
          });
        }
      }
    })
  },
  globalData: {
    userInfo: null,
    token: null
  },
  touches: new Touches(),
  request: require("utils/request.js"),
  util:require("utils/util.js")
})