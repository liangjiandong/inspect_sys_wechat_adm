const App = getApp()
import itemData from './mock.js'

Page({
  data: {
    itemData,
    showDeleteModal:false,
    showAddModal:false
  },
  showDeleteClassifyModal:function(){
  this.setData({
    showDeleteModal:true
   });
  },
  deleteClassify:function(){
  },
  showAddClassifyModal:function(){
    this.setData({
      showAddModal: true
    });
  },
  addClassify: function () {

  },
  /**
   * 跳转到巡查项列表页面
   */
  goItemListPage:function(){
    wx.navigateTo({
      url: 'item/item',
    });
  },
  goClassifyContentPage:function(){
    wx.navigateTo({
      url: 'classify-content/classify-content',
    })
  },
  onLoad: function (options) {
    // 页面初始化 options为页面跳转所带来的参数
  },
  onReady: function () {
    // 页面渲染完成
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  }
})