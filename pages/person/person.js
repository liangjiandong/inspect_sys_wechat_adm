import PageUtils from '../../utils/PageUtils.js';
 Page({
  data: {
    itemData:[]
  },
  pageData:new PageUtils('/inspect/usr/list'),
  onPullDownRefresh: function () {
    let that = this;
    this.pageData.pullDownRefresh([]).then(dataList=>{
      that.setData({
        itemData: dataList
      });
    });
  },
  /**
 * 页面上拉触底事件的处理函数
 */
  onReachBottom: function () {
    let that = this;
    this.pageData.reachBottom(this.data.itemData).then(dataList => {
      that.setData({
        itemData: dataList
      });
    });
  },
  onLoad: function (options) {
    let that = this;
    this.pageData.getDataList([]).then(dataList => {
      that.setData({
        itemData: dataList
      });
    });;
  },
  onReady: function () {
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  }
})