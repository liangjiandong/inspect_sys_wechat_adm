const app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    phoneNumber: '',
    validateCode: '',
    codeButton:{
      disable:false,
      value:'获取验证码',
      currentTime:120
    }
  },
  inputPhone: function(e) {
    this.setData({
      phoneNumber: e.detail.value
    });
  },
  inputCode: function(e) {
    this.setData({
      validateCode: e.detail.value
    });
  },
  /** 登录 */
  login: function(e) {
    //判断手机号格式是否正确
    if(!app.util.validatePhoneFormat(this.data.phoneNumber)){
      app.util.showToast("请输入正确的手机号");
      return;
    }
    let usrInfo = e.detail.userInfo;
    // 登录
    wx.login({
      success: res => {
        let params = {
          "nickName": usrInfo.nickName,
          "avatarUrl": usrInfo.avatarUrl,
          "code": res.code,
          "phoneNumber": this.data.phoneNumber,
          "validateCode": this.data.validateCode
        };
        app.request.postJson("auth/phone/login", params, null).then(data => {
          app.globalData.token = data;
          wx.switchTab({
            url: "../index/index",
          });
        });
      }
    })
  },
  /**
   * 获取验证码
   */
  getValidateCode(){
    //判断手机号格式是否正确
    if (!app.util.validatePhoneFormat(this.data.phoneNumber)) {
      app.util.showToast("请输入正确的手机号");
      return;
    }
    //发送通知
    app.request.postJson("notice/code/"+this.data.phoneNumber).then(data=>{
      if(data != null){
        this.waiting();
      }
    }); 
  },
  waiting(){
    let _this = this;
    let interval = null;
    let currentTime = this.data.codeButton.currentTime;

    interval = setInterval(function () {
      currentTime--;
      _this.setData({
        codeButton: {
          currentTime: currentTime,
          value: currentTime + "秒后重新获取",
          disable: true
        }
      });
      if (currentTime <= 0) {
        clearInterval(interval);
        _this.setData({
          codeButton: {
            currentTime: 120,
            value: "重新发送",
            disable: false
          }
        });
      }
    }, 1000);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})